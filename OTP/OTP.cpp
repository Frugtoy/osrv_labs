#include <cstdlib>
#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include<vector>
#define NUMBER_OF_ARGS 13
#define ERROR_IN_STD_FUNK -1
#define MAX_FILE_SIZE 50000

using std::vector;
using std::cout;

enum err{
    ERR_IN_ARGS = -127,
    ERR_IN_INPUT_FILE,
    ERR_IN_OUTPUT_FILE,
    ERR_IN_CREATING_THREAD,
    ERR_IN_JOIN_THREAD,
    ERR_IN_CREATIN_BARRIER,
    ERR_IN_WRITE,
    ERR_TOO_HEAVY_FILE,
    UNEXPECT_ERROR
}err_type; //для обработчика исключений;


//otp  -i /path/to/text.txt -o -/path/to/cypher.txt -x0 4212 -a 84589 -c 45989 -m 217728
struct commandLineParams
{
    char *pathToInputFile;//-i [...]
    char *pathToOutputFile;//-o[...]
    size_t x;
    size_t a;
    size_t c;
    size_t m;

};

struct file
{
    char *filePath;
    int descriptor;
    int size;
};

struct keyGeneratorArg
{
    size_t a;
	size_t c;
	size_t m;
	size_t x;
	size_t keySize;

};

struct cryptParamContext//6
{
	char* msg;
	char* key;
	char* outputText;
	size_t size;
	size_t downIndex;
	size_t topIndex;
	pthread_barrier_t* barrier;
};

commandLineParams* parse_arg(int argc, char ** argv)
{
    commandLineParams* result = new commandLineParams;
    int flag;
    if(argc < NUMBER_OF_ARGS){
        throw(ERR_IN_ARGS);
        delete result;
        return NULL;
    }
    	while ((flag = getopt(argc, argv, "i:o:a:c:x:m:")) != -1) 
        {
		    switch (flag) 
            {
		        case 'i':
			        cout<<"-i [" << optarg<<"]\n";
			        result->pathToInputFile = optarg;
                    break;
		        case 'o':
			        cout<<"-o [" << optarg<<"]\n";
			        result->pathToOutputFile = optarg;
		    	    break;
		        case 'a':
			        cout<<"-a [" << optarg<<"]\n";
			        result->a = atoi(optarg);
			        break;
		        case 'c':
			        cout<<"-c [" << optarg<<"]\n";
			        result->c = atoi(optarg);
			        break;
		        case 'm':
                    cout<<"-m [" << optarg<<"]\n";
			        result->m = atoi(optarg);
			        break;
		        case 'x':
			        cout<<"-x [" << optarg<<"]\n";
			        result->x = atoi(optarg);
			        break;
		        case '?':
			        break;

		        default:
			        cout<<"there is some unexpected argument[" << optarg<<"]\n";
                    break;
		    }      
	}
    if (optind < argc || argc == 0) 
    {
		delete result;
        throw(ERR_IN_ARGS);
	}
    return result;
}

char* getInputFileBuffer(file * inputFile, commandLineParams * args )
{
    inputFile->descriptor = open(args->pathToInputFile,O_RDONLY);
    if(inputFile->descriptor == ERROR_IN_STD_FUNK) 
    {
        throw(ERR_IN_INPUT_FILE);
    }
    inputFile->size = lseek(inputFile->descriptor,0,SEEK_END);
    if(inputFile->size == ERROR_IN_STD_FUNK) 
    {   
        throw(ERR_IN_INPUT_FILE);
    }
    else if (inputFile->size > MAX_FILE_SIZE) 
    {   
        throw(ERR_TOO_HEAVY_FILE);
    }
    lseek(inputFile->descriptor, 0, SEEK_SET);
    char * buf = new char[inputFile->size];
    inputFile->size = read(inputFile->descriptor,buf,inputFile->size);
    
    if(inputFile->size == ERROR_IN_STD_FUNK) 
    {   
        throw(ERR_IN_INPUT_FILE);
    }
    
    close(inputFile->descriptor);
    return buf;
}
//routines
void* generatorRoutine(void* argument)
{
    keyGeneratorArg *parametrs = reinterpret_cast<keyGeneratorArg*>(argument);
    size_t a = parametrs->a;
    size_t m = parametrs->m;
    size_t c = parametrs->c;
    size_t sizeKey = parametrs->keySize;

    int* buff = new int[sizeKey/sizeof(int) + 1];
    buff[0] = parametrs->x;

    for(size_t i = 1; i < sizeKey/sizeof(int) + 1 ; i++){
        buff[i]= (a * buff[i-1] + c) % m;
    }

    return reinterpret_cast<char *>(buff);

}
void* cryptoThreadRoutine(void* argument)
{
    //cout<<"crypto thread openned\n";
    cryptParamContext* _arg = reinterpret_cast<cryptParamContext*>(argument);
    size_t finish = _arg->topIndex;
    size_t nowAt = _arg->downIndex;
    
    while(nowAt < finish)
    {
        _arg->outputText[nowAt] = _arg->key[nowAt] ^ _arg->msg[nowAt];
        nowAt++;
    }
    //cout<<"barrier\n";
    int barrierStatus = pthread_barrier_wait(_arg->barrier);
    if(barrierStatus != PTHREAD_BARRIER_SERIAL_THREAD && barrierStatus != 0)
    {
        throw(ERR_IN_CREATIN_BARRIER);
    }
}


int main(int argc, char **argv)
{
    int* error_temp = new int;
    commandLineParams *args;
    file *inputFile = new file;
    file *outputFile = new file;

    char *buff, *key, *outputText;

    int number_of_threads = _SC_NPROCESSORS_ONLN +1;
    pthread_t keyGenerator;
    keyGeneratorArg _keyGeneratorArg;

    pthread_t cryptoThreads[number_of_threads];
    pthread_barrier_t barrier;
    int barrier_status = 0;

    //cryptParamContext ** _cryptoParamContext = new cryptParamContext*[_SC_NPROCESSORS_ONLN];
    vector<cryptParamContext*> _cryptoParamContext;
   

    try
    {
        args  = parse_arg(argc,argv);//1
        buff = getInputFileBuffer(inputFile,args);//2

        key = new char[inputFile->size];
        outputText = new char[inputFile->size];

        _keyGeneratorArg.a = args->a;
        _keyGeneratorArg.c = args->c;
        _keyGeneratorArg.m = args->m;
        _keyGeneratorArg.x = args->x;

        std::cout << "file size: "<<inputFile->size << std::endl;
        _keyGeneratorArg.keySize = inputFile->size;

        if(pthread_create(&keyGenerator,NULL,generatorRoutine,&_keyGeneratorArg) != 0) 
        {   
            throw(ERR_IN_CREATING_THREAD);
        }
        
        if(pthread_join(keyGenerator,(void **)&key )!=0)
        {   
            throw(ERR_IN_JOIN_THREAD);
        }
        barrier_status = pthread_barrier_init(&barrier, NULL, number_of_threads);
        
        if(barrier_status != 0 )
        {   
            throw(ERR_IN_CREATIN_BARRIER);
        }
        
        for(int i = 0; i < number_of_threads- 1; i++)
	    {
            cryptParamContext * worker = new cryptParamContext;
		    worker->key = key;
		    worker->size = inputFile->size;
		    worker->outputText = outputText;
		    worker->msg = buff;
		    worker->barrier = &barrier;
            worker->downIndex =  i * worker->size / (number_of_threads - 1);
            worker->topIndex  =  (i + 1) * worker->size / (number_of_threads - 1);
            if(i == number_of_threads -2){
                worker->topIndex = worker->size;
                cout<<(worker->size % (number_of_threads - 1))<<"\n";
            } 
            cout<<"["<<worker->downIndex<<"]["<<worker->topIndex<<"]\n";
            _cryptoParamContext.push_back(worker);
		    pthread_create(&cryptoThreads[i], NULL, cryptoThreadRoutine, worker);
	    }
        pthread_barrier_wait(&barrier);

        
        if ((outputFile->descriptor=open(args->pathToOutputFile, O_WRONLY))==-1) 
        {
            close(outputFile->descriptor);
            throw(ERR_IN_OUTPUT_FILE);
        }
        if(write(outputFile->descriptor, outputText, inputFile->size) !=inputFile->size)
        {   
            close(outputFile->descriptor);
            throw(ERR_IN_WRITE);
        }
        
        close(outputFile->descriptor);
        
    }
    catch(err err_type){
        switch (err_type)
        {
            case ERR_IN_ARGS:
                cout<<"ERR_IN_ARGS\n";
                close(inputFile->descriptor);
                delete inputFile;
                delete outputFile;
                break;
            case ERR_IN_INPUT_FILE:
                cout<<"ERR_IN_INPUT_FILE\n";
                close(inputFile->descriptor);
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                break;
            case ERR_IN_CREATING_THREAD:
                cout<<"ERR_IN_CREATING_THREAD\n";
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                delete key;               
                break;
            case ERR_IN_JOIN_THREAD:
                cout<<"ERR_IN_JOIN_THREAD\n";
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                delete key;
                break;
            case ERR_IN_CREATIN_BARRIER:
                cout<<"ERR_IN_CREATIN_BARRIER\n";
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                delete key;
                break;
            case ERR_IN_OUTPUT_FILE:
                cout<< "ERR_IN_OUTPUT_FILE";
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                delete key;
                for(int i = 0; i < _cryptoParamContext.size(); i++) {    
                    delete _cryptoParamContext[i];
                }
                
                break;
            case ERR_IN_WRITE:
                cout<<"ERR_IN_WRITE\n";
                delete args;
                delete inputFile;
                delete outputFile;
                delete buff;
                delete key;
                delete outputText;                   
                for(int i = 0; i < _cryptoParamContext.size(); i++) {    
                    delete _cryptoParamContext[i];
                }
                break;
            case ERR_TOO_HEAVY_FILE:
                cout<<"ERR_TOO_HEAVY_FILE\n";
                close(inputFile->descriptor);
                delete args;
                delete inputFile;
                delete outputFile;
                break;
        }
        return err_type;
    }

    delete args;
    delete inputFile;
    delete outputFile;
    delete[] outputText;
    delete[] buff;
    delete[] key;

    for(int i = 0; i < _SC_NPROCESSORS_ONLN; i++) {    
        delete _cryptoParamContext[i];
    }

    
    return 0;
}